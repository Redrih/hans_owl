<?php

use Illuminate\Support\Facades\Route;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\user\ConnectionsController;
use App\Http\Controllers\user\TariffsController;
use App\Http\Controllers\admin\ConnectionController;
use App\Http\Controllers\admin\HouseController;
use App\Http\Controllers\admin\TariffController;
use App\Http\Controllers\admin\UserController;
use App\Http\Controllers\admin\ClientQueryController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

#main page routes

Route::get('/main', function() {
    $role = Auth::user()->role ;
        if ($role == 'client') {
            return view('provider.main');
        } else if (in_array($role, ['admin', 'moder_tariff', 'moder_connection'])) {
            return Redirect::to('/admin/main');
        } else {
            return view('provider.tariffs.tariffs');
        }
});
Route::redirect('/', 'tariffs');

#admin routes

Route::prefix('/admin')->group(function() {
    
    Route::resource('/houses', HouseController::class)
            ->except(['create', 'store', 'destroy'])
            ->middleware('role:admin|moder_tariff|moder_connection');
    
    Route::resource('/tariffs', TariffController::class)
            ->middleware('role:admin|moder_tariff');
    
    Route::resource('/users', UserController::class)
            ->except(['create', 'store', 'destroy'])
            ->middleware('role:admin');
    
    Route::middleware('role:admin|moder_connection')->group(function() {
        Route::resource('/connections', ConnectionController::class);
        Route::get('/queries', [ClientQueryController::class, 'show']);
        Route::put('/queries/perform/{id}', [ClientQueryController::class, 'perform']);
    });
    
    Route::view('/main', 'admin.main');
});

#extra routes

Route::view('/petya', 'petya');

#client's connections routes

Route::middleware('role:client') -> group (function() {
    Route::prefix('/connections')->group(function() {
        Route::get('/',
            [ConnectionsController::class, 'showConnectionsByClientId']
            );
        Route::get('/id={id}',
            [ConnectionsController::class, 'showConnectionSingleById']
            );
        Route::get('/connect', [ConnectionsController::class, 'askForConnection']);
        Route::get('/disconnect', [ConnectionsController::class, 'askForDisconnection']);
        Route::get('/edit/{id}', [ConnectionsController::class, 'askForEditingConnection']);
        Route::put('/store/{type}', [ConnectionsController::class, 'storeQuery']);
    });
});

#tariffs routes

Route::get('/tariffs',
        [TariffsController::class, 'showTariffsAll']
        );

Route::get('/tariffs/id={id}',
        [TariffsController::class, 'showTariffSingleById']
        );

#authentication routes

Auth::routes([
    'register' => true,
    'verify' => false,
    'reset' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
