<?php

namespace App\Models\user;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Tariffs extends Model
{
    use HasFactory;
    
    public static function getTariffsAll() {
        return DB::table('tariff')
                ->select('tariff_id as ID',
                        'name as Name',
                        'month_price as Month_price',
                        'conditions as Conditions')
                ->get();
    }
    
     public static function getTariffSingleById($id) {
        return DB::table('tariff')
                ->select('tariff_id as ID',
                'name as Name',
                'month_price as Month_price',
                'conditions as Conditions')
                ->where('tariff_id', '=', $id)
                ->get()->first();
     }
}
