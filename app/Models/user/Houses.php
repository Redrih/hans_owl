<?php

namespace App\Models\user;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class Houses extends Model {
    
    use HasFactory;
    
    public static function getHousesAll() {
        return DB::table('house')
                ->select('street as Street', 'house_number as House_number', 'house_id as ID')
                ->orderBy('street', 'asc')
                ->get();
    }
}