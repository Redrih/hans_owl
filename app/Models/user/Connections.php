<?php

namespace App\Models\user;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Connections extends Model
{
    use HasFactory;
    
    public static function getConnectionsByClient($client_id) {
        return DB::table('connection')
                ->join('house', 'connection.house_id', '=', 'house.house_id')
                ->join('tariff', 'connection.tariff_id', '=', 'tariff.tariff_id')
                ->select('connection.connection_id as ID',
                        'connection.client_id as Client_ID',
                        'house.street as Street',
                        'house.house_number as House_number',
                        'connection.house_id as House_ID',
                        'connection.flat as Flat',
                        'connection.tariff_id as Tariff_ID',
                        'tariff.name as Tariff_name',
                        'tariff.month_price as Month_price',
                        'tariff.conditions as Conditions')
                ->where('connection.client_id', '=', $client_id)
                ->orderBy('connection.tariff_id')
                ->get();
    }
    
    public static function getConnectionSingleById($id) {
        return DB::table('connection')
                ->join('house', 'connection.house_id', '=', 'house.house_id')
                ->join('tariff', 'connection.tariff_id', '=', 'tariff.tariff_id')
                ->select('connection.connection_id as ID',
                        'connection.client_id as Client_ID',
                        'house.street as Street',
                        'house.house_number as House_number',
                        'house.house_id as House_ID',
                        'connection.flat as Flat',
                        'connection.tariff_id as Tariff_ID',
                        'tariff.name as Tariff_name',
                        'tariff.month_price as Month_price',
                        'tariff.conditions as Conditions')
                ->where('connection.connection_id', '=', $id)
                ->get()->first();
    }
    
    public static function sendQuery(Request $request, $type) {
        switch($type) {
            case 'connect':
                if ($request->input('house_type') == 'old'){
                    DB::table('client_query')
                        ->insert([
                            'client_id' => Auth::user()->getAuthIdentifier(),
                            'house_id' => $request->input('house_id'),
                            'flat' => $request->input('flat'),
                            'tariff_id' => $request->input('tariff_id'),
                            'query_type' => 'connect'
                        ]);
                } elseif ($request->input('house_type') == 'new') {
                    DB::table('client_query')
                        ->insert([
                            'client_id' => Auth::user()->getAuthIdentifier(),
                            'street' => $request->input('street'),
                            'house_number' => $request->input('house_number'),
                            'flat' => $request->input('flat'),
                            'tariff_id' => $request->input('tariff_id'),
                            'query_type' => 'connect'
                        ]);
                }
                break;
            case 'disconnect':
                $client_id = Auth::user()->getAuthIdentifier();
                foreach (Connections::getConnectionsByClient($client_id) as $connection) {
                    $id = $connection->ID;
                    if ($request->input((string)$id)) {
                        $to_disconnect = $request->input($connection->ID);
                        DB::table('client_query')
                            ->insert([
                                'client_id' => $client_id,
                                'connection_id' => $id,
                                'house_id' => $connection->House_ID,
                                'tariff_id' => $connection->Tariff_ID,
                                'query_type' => 'disconnect'
                            ]);
                    }
                }
                break;
            case 'edit':
                if ($request->input('house_type') == 'old'){
                    DB::table('client_query')
                        ->insert([
                            'client_id' => Auth::user()->getAuthIdentifier(),
                            'connection_id' => $request->input('connection_id'),
                            'house_id' => $request->input('house_id'),
                            'flat' => $request->input('flat'),
                            'tariff_id' => $request->input('tariff_id'),
                            'query_type' => 'edit'
                        ]);
                } elseif ($request->input('house_type') == 'new') {
                    DB::table('client_query')
                        ->insert([
                            'client_id' => Auth::user()->getAuthIdentifier(),
                            'connection_id' => $request->input('connection_id'),
                            'street' => $request->input('street'),
                            'house_number' => $request->input('house_number'),
                            'flat' => $request->input('flat'),
                            'tariff_id' => $request->input('tariff_id'),
                            'query_type' => 'edit'
                        ]);
                }
                break;
        }
    }
}
