<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\admin\_SysAdmin;
use Illuminate\Support\Facades\DB;

class User extends _SysAdmin
{
    use HasFactory;
    
    
    public static function get($column = 0, $direction = 'asc') {
        $ordering_options = User::_getSortingOptions()['implicit'];
        $column_prepared = in_array($column, [0, 1, 2])? $column : 0;
        $direction_prepared = in_array($direction, ['asc', 'desc']) ? $direction : 'asc' ;
        $users = DB::table('users')
                ->select('id as ID',
                        'name as Login',
                        'email as Email',
                        'role as Role')
                ->orderBy($ordering_options[$column_prepared], $direction_prepared)
                ->get();
        foreach ($users as $user) {
            if ($user->Role == 'client') {
                $user->Connections_count = DB::table('connection')
                   ->where('client_id', '=', $user->ID)
                   ->count();
            } else {
                $user->Connections_count = '-';
            }
        }
        
        return $users;
    }
    
    public static function getUserSingleByID($id) {
        return DB::table('users')
                ->select('users.id as ID',
                        'users.name as Login',
                        'users.email as Email',
                        'users.role as Role')
                ->where('id', '=', $id)
                ->get()->first();
    }

    public static function put(array $data= array()) {
        return null;
    }

    public static function update_row(array $data = array(), array $options = array()) {
        DB::table('users')
                ->where('id', '=', $data['id'])
                ->update([
                    'role' => $data['role']
                ]);
    }
    
    public static function destroy($id) {
        return null;
    }

    public static function _getColumnsToAdd() {
        return [];
    }
    
    public static function _getDataSecondaryToAdd() {
        return[];
    }
    
     public static function _getDataFieldsToAdd() {
        return [];
    }
    
    public static function _getOptionsFieldsToAdd() {
        return [];
    }

    public static function _getColumnsToShow() {
        return [
            'ID', 'Login', 'Email', 'Role', 'Connections_count'
        ];
    }
    
    public static function _getSortingOptions() {
        return [
            'explicit' => ['ID', 'Login', 'Role'],
            'implicit' => ['users.id', 'users.name', 'users.role']
        ];
    }
    
    public static function _getColumnsToEdit() {
        return [
            'ID' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text', 'maxlength' => '4',
                'title' =>[], 'has_radio' => false],
            'Login' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text', 'maxlength' => '30',
                'title' =>[], 'has_radio' => false],
            'Email' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text', 'maxlength' => '100',
                'title' =>[], 'has_radio' => false],
            'Role' => ['editability' => 'editable', 'tag' => 'select',
                'title' =>[], 'has_radio' => false],
        ];
    }

    public static function _getDataFieldsToEdit() {
        return ['id', 'role'];
    }
    
    public static function _getDataSecondaryToEdit() {
        $roles = [
            ['ID' => 'moder_tariff' , 'to_view' => 'moder_tariff', 'title' => ''],
            ['ID' => 'moder_connection' , 'to_view' => 'moder_connection', 'title' => ''],
            ['ID' => 'client' , 'to_view' => 'client', 'title' => '']
            ];
        return [
            'for_select_tag' => ['Role' => $roles]
        ];
    }

    public static function _getOptionsFieldsToEdit() {
        return [];
    }

}
