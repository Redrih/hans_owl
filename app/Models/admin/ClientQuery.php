<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\admin\Connection;

class ClientQuery extends Model
{
    use HasFactory;
    
    public static function getFirst() {
        $query = DB::table('client_query')
                        ->select('client_id as Client_ID',
                                'query_id as Query_ID',
                                'connection_id as Connection_ID',
                                'house_id as House_ID',
                                'street as Street',
                                'house_number as House_number',
                                'flat as Flat',
                                'tariff_id as Tariff_ID',
                                'query_type as Query_type')
                        ->orderBy('client_query.created_at', 'asc')
                        ->get()->first();
        ClientQuery::getAdditionalDataForQuery($query);
        return $query;
    }
    
    public static function getOneById($id) {
        $query = DB::table('client_query')
                        ->select('client_id as Client_ID',
                                'query_id as Query_ID',
                                'connection_id as Connection_ID',
                                'house_id as House_ID',
                                'street as Street',
                                'house_number as House_number',
                                'flat as Flat',
                                'tariff_id as Tariff_ID',
                                'query_type as Query_type')
                        ->where('query_id', '=', $id)
                        ->get()->first();
        ClientQuery::getAdditionalDataForQuery($query);
        return $query;
    }
    
    private static function getAdditionalDataForQuery($query) {
        if ($query) {
            $query->Login = DB::table('users')
                ->select('name as Login')
                ->where('id', '=', $query->Client_ID)
                ->get()->first()->Login;
            if ($query->Connection_ID) {
                $query->Old_tariff = DB::table('connection')
                        ->select('tariff_id')
                        ->where('connection_id', '=', $query->Connection_ID)
                        ->get()->first()->tariff_id;
            }
            if (($query->House_ID || $query->Street) && $query->Connection_ID) {
                $query->Old_house = DB::table('connection')
                        ->select('house_id')
                        ->where('connection_id', '=', $query->Connection_ID)
                        ->get()->first()->house_id;
            }
        }
    }
    
    public static function destroy($id) {
        DB::table('client_query')
                ->where('query_id', '=', $id)
                ->delete();
    }
    
    public static function perform($id, $answer) {
        if ($answer == 'accept') {
            $query = ClientQuery::getOneById($id);
            switch ($query->Query_type) {
                case 'connect':
                    $data = array();
                    $options = array();
                    $data['client_id'] = $query->Client_ID;
                    $data['flat'] = $query->Flat;
                    $data['tariff_id'] = (string)$query->Tariff_ID;
                    if ($query->House_ID) {
                        $data['house_id'] = $query->House_ID;
                        $options['house_type'] = 'old';
                    } else {
                        $data['street_new'] = $query->Street;
                        $data['house_number'] = $query->House_number;
                        $options['house_type'] = 'new';
                    }
                    DB::transaction( function() use ($data, $options, $id) {
                        Connection::put($data, $options);
                        ClientQuery::destroy($id);
                    });
                    break;
                case 'disconnect':
                    DB::transaction( function() use ($id, $query) {
                        Connection::destroy($query->Connection_ID);
                        ClientQuery::destroy($id);
                    });
                    break;
                case 'edit':
                    $data = array();
                    $options = array();
                    $data['client_id'] = $query->Client_ID;
                    $data['connection_id'] = $query->Connection_ID;
                    $data['flat'] = $query->Flat;
                    $data['tariff_id'] = $query->Tariff_ID;
                    if ($query->House_ID) {
                        $data['house_id'] = $query->House_ID;
                        $options['house_type'] = 'old';
                    } else {
                        $data['street_new'] = $query->Street;
                        $data['house_number'] = $query->House_number;
                        $options['house_type'] = 'new';
                    }
                    DB::transaction( function() use ($data, $options, $query) {
                        Connection::update_row($data, $options);
                        ClientQuery::destroy($query->Query_ID);
                    });
                    break;
            }
        } elseif ($answer == 'decline') {
            ClientQuery::destroy($id);
        }
    }
}
