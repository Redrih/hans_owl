<?php

namespace App\Models\admin;

use Illuminate\Http\Request;
/**
 *
 * @author Admin
 */
interface _AdminInterface {
    public static function _splitRequest(
            Request $request,
            array $data_fields = Array(),
            array $options_fields = Array()
            );
    
    #for showing
    public static function _getColumnsToShow();
    public static function _getSortingOptions();
    
    #for editing
    public static function _getColumnsToEdit();
    public static function _getDataSecondaryToEdit();
    public static function _getDataFieldsToEdit();
    public static function _getOptionsFieldsToEdit();
    
    #for adding
    public static function _getColumnsToAdd();
    public static function _getDataSecondaryToAdd();
    public static function _getDataFieldsToAdd();
    public static function _getOptionsFieldsToAdd();
}
