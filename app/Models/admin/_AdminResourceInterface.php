<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models\admin;

/**
 *
 * @author Admin
 */
interface _AdminResourceInterface {
    
    public static function get($column = 0, $direction = 'asc');
    public static function put(array $data);
    public static function update_row(array $data = Array(), array $options = Array());
    public static function destroy($id);
}
