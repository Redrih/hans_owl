<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use App\Models\user\Houses;
use App\Models\user\Tariffs;

class Connection extends _SysAdmin
{
    use HasFactory;

    public static function get($column = 0, $direction = 'asc') {
        $ordering_options = Connection::_getSortingOptions()['implicit'];
        $column_prepared = in_array($column, [0, 1, 2, 3])? $column : 0;
        $direction_prepared = in_array($direction, ['asc', 'desc'])? $direction : 'asc';
        return DB::table('connection')
                ->join('house', 'connection.house_id', '=', 'house.house_id')
                ->join('tariff', 'connection.tariff_id', '=', 'tariff.tariff_id')
                ->join('users', 'connection.client_id', '=', 'users.id')
                ->select('connection.connection_id as ID',
                        'connection.client_id as Client_ID',
                        'users.name as Login',
                        'house.street as Street',
                        'house.house_number as House_number',
                        'connection.house_id as House_ID',
                        'connection.tariff_id as Tariff_ID',
                        'tariff.name as Tariff_name',
                        'tariff.month_price as Price',
                        'tariff.conditions as Conditions')
                ->orderBy($ordering_options[$column_prepared], $direction_prepared)
                ->orderBy('house.house_number', 'asc')
                ->get();
    }

    public static function getOneById($id) {
        return DB::table('connection')
                ->join('users', 'connection.client_id', '=', 'users.id')
                ->join('house', 'connection.house_id', '=', 'house.house_id')
                ->join('tariff', 'connection.tariff_id', '=', 'tariff.tariff_id')
                ->select('connection.connection_id as ID',
                        'connection.client_id as Client_ID',
                        'users.name as Login',
                        'house.street as Street',
                        'house.house_number as House_number',
                        'house.house_id as House_ID',
                        'connection.flat as Flat',
                        'connection.tariff_id as Tariff_ID',
                        'tariff.name as Name',
                        'tariff.month_price as Month_price',
                        'tariff.conditions as Conditions')
                ->where('connection.connection_id', '=', $id)
                ->get()->first();
    }

    public static function getAllByHouseId($id) {
        return DB::table('connection')
                ->join('users', 'connection.client_id', '=', 'users.id')
                ->join('house', 'connection.house_id', '=', 'house.house_id')
                ->join('tariff', 'connection.tariff_id', '=', 'tariff.tariff_id')
                ->select('connection.connection_id as ID',
                        'connection.client_id as Client_ID',
                        'users.name as Login',
                        'connection.flat as Flat',
                        'connection.tariff_id as Tariff_ID',
                        'tariff.name as Name',
                        'tariff.month_price as Month_price',
                        'tariff.conditions as Conditions')
                ->where('connection.house_id', '=', $id)
                ->get();
    }

    public static function put(array $data = Array(), array $options = Array()) {
        switch ($options['house_type']) {
            case 'old':
                DB::table('connection')
                    ->insert([
                        'client_id' => $data['client_id'],
                        'house_id' => $data['house_id'],
                        'flat' => $data['flat'],
                        'tariff_id' => $data['tariff_id']
                    ]);
                break;
            case 'new':
                if (!empty($data['street_new'])) {
                    DB::transaction(function() use ($data) {
                        DB::table('house')
                                ->insert([
                                    'street' => $data['street_new'],
                                    'house_number' => $data['house_number']
                                ]);
                        $house_id = DB::table('house')
                                ->max('house_id');
                        DB::table('connection')
                            ->insert([
                                'client_id' => $data['client_id'],
                                'house_id' => $house_id,
                                'flat' => $data['flat'],
                                'tariff_id' => $data['tariff_id']
                            ]);
                    });

                }
                break;
        }
    }

    public static function update_row(array $data = Array(), array $options = Array()) {
        switch ($options['house_type']) {
            case 'old':
                DB::table('connection')
                    ->where('connection_id', '=', $data['connection_id'])
                    ->update([
                        'house_id' => $data['house_id'],
                        'flat' => $data['flat'],
                        'tariff_id' => $data['tariff_id']
                    ]);
                break;
            case 'new':
                if (!empty($data['street_new'])) {
                    DB::transaction(function() use ($data) {
                        if (House::checkHousePresence($data['street_new'], $data['house_number'])){
                            DB::table('house')
                                ->insert([
                                    'street' => $data['street_new'],
                                    'house_number' => $data['house_number']
                                ]);
                        }
                        $house_id = DB::table('house')
                                ->max('house_id');
                        DB::table('connection')
                            ->where('connection_id', '=', $data['connection_id'])
                            ->update([
                                'house_id' => $house_id,
                                'flat' => $data['flat'],
                                'tariff_id' => $data['tariff_id']
                            ]);
                    });
                }
                break;
            default:
                DB::table('connection')
                    ->where('connection_id', '=', $data['connection_id'])
                    ->update([
                        'flat' => $data['flat'],
                        'tariff_id' => $data['tariff_id']
                    ]);
        }
    }

    public static function destroy($id) {
        DB::transaction(function() use ($id) {
            $house_id = DB::table('connection')
                ->select('house_id as House_ID')
                ->where('connection_id', '=', $id)
                ->get()->first()->House_ID;
            DB::table('connection')
                    ->where('connection_id', '=', $id)
                    ->delete();
            $connections_remain = DB::table('connection')
                    ->where('house_id', '=', $house_id)
                    ->count();
            if ($connections_remain == 0){
                DB::table('house')
                        ->where('house_id', '=', $house_id)
                        ->delete();
            }
        });
    }

    public static function _getColumnsToShow() {
        return [
            'ID', 'Login', 'Street',
            'House_number', 'House_ID',
            'Tariff_ID', 'Tariff_name', 'Price'
        ];
    }

    /*used for showing the whole list of connecitons
     * explicit for output option text
     * implicit for get() method
     */
    public static function _getSortingOptions() {
        return [
            'explicit' => ['connection ID', 'client\'s login', 'street', 'tariff'],
            'implicit' => ['connection.connection_id',
                'users.name',
                'house.street',
                'tariff.tariff_id']
            ];
    }

    public static function _getColumnsToEdit() {
        return [
            /*
             * editability,     tag,     type (for input tag),      title,      has_radio (+radio if has)
             */
            'ID' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text',
                'title' =>[], 'has_radio' => false],
            'Client_ID' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text',
                'title' => ['Login', 'Balance'], 'has_radio' => false],
            'House_ID' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text',
                'title' => ['Street', 'House_number'], 'has_radio' => true
                ],
            'Tariff_ID' => ['editability' => 'editable', 'tag' => 'select', 'type' => 'text',
                'title' => ['Tariff_name', 'Price', 'Conditions'], 'has_radio' => false]
        ];
    }

    public static function _getDataFieldsToEdit() {
        return [
            'connection_id', 'client_login', 'house_id', 'house_number', 'street_new', 'flat', 'tariff_id'
        ];
    }

    private static function getTariffsList() {
        $tariffs_raw = Tariffs::getTariffsAll();
        $tariffs = array();
        $iter = 0;
        foreach ($tariffs_raw as $tariff){
            $tariffs[$iter]['ID'] = $tariff->ID;
            $tariffs[$iter]['title'] = "name: $tariff->Name\n" .
                    "month price: $tariff->Price\n" .
                    "conditions: $tariff->Conditions";
            $tariffs[$iter]['to_show'] = $tariff->Name;
            $iter ++;
        }
        return $tariffs;
    }

    private static function getHousesList() {
        $houses_raw = Houses::getHousesAll();
        $houses = array();
        $select_house_option_values = array();
        $select_house_option_text = array();
        $iter = 0;
        foreach ($houses_raw as $house) {
            $houses[$iter]['ID'] = $house->ID;
            $houses[$iter]['to_view'] = $house->Street . $house->House_number;
            $houses[$iter]['title'] = "street: $house->Street\n".
                    "number: $house->House_number";
            $select_house_option_values[] = $houses[$iter]['to_view'];
            $select_house_option_text[] = $houses[$iter]['ID'];
            $iter ++;
        }
        return ['houses' => $houses,
            'option_values' => $select_house_option_values,
            'option_text' => $select_house_option_text];
    }


    public static function _getDataSecondaryToEdit() {
        $tariffs = Connection::getTariffsList();
        $houses_raw = Connection::getHousesList();
        $houses = $houses_raw['houses'];
        $house_option_values = $houses_raw['option_values'];
        $house_option_text = $houses_raw['option_text'];
        return [
            'for_select_tag' => ['Tariff_ID' => $tariffs,
                'House_ID' => $houses],
            'for_radio_tag' => ['House_ID' => [
                ['name' => 'house_type', 'value' => 'old', 'label' => 'choose from the list', 'tag' => 'select',
                    'option_values' => $house_option_values,
                    'option_text' => $house_option_text],
                ['name' => 'house_type', 'value' => 'new', 'label' => 'add new one', 'tag' => 'input',
                    'input_tags' => [
                        ['label' => 'street', 'type' => 'text'],
                        ['label' => 'house_number', 'type' => 'number', 'min' => '1', 'max' => '50']
                        ],
                    ]
            ]],
            'for_input_tag' => []
        ];
    }

    public static function _getOptionsFieldsToEdit() {
        return [
            'house_type'
        ];
    }

    public static function _getColumnsToAdd() {
        return [];
    }

    public static function _getDataSecondaryToAdd(){
        return [];
    }

    public static function _getDataFieldsToAdd() {
        return [];
    }

    public static function _getOptionsFieldsToAdd() {
        return [];
    }

}
