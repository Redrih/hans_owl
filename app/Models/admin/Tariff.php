<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class Tariff extends _SysAdmin
{
    use HasFactory;

    public static function get($column = 0, $direction = 'asc') {
        $ordering_options = Tariff::_getSortingOptions()['implicit'];
        $column_prepared = in_array($column, [0, 1, 2])? $column : 0;
        $direction_prepared = in_array($direction, ['asc', 'desc']) ? $direction : 'asc' ;
        return DB::table('tariff')
                ->select('tariff.tariff_id as ID',
                        'tariff.name as Name',
                        'tariff.month_price as Month_price',
                        'tariff.conditions as Conditions')
                ->orderBy($ordering_options[$column_prepared], $direction_prepared)
                ->get();
    }

    public static function getOneByID($id) {
        return DB::table('tariff')
                ->select('tariff_id as ID',
                        'name as Name',
                        'month_price as Month_price',
                        'conditions as Conditions')
                ->where('tariff_id', '=', $id)
                ->get()->first();
    }

    private static function generateID($tariff_name) {
        $name_pattern = explode('_', $tariff_name)[0];
        $last_similiar_tariff = DB::table('tariff')
                ->select('tariff_id as ID')
                ->where('name', 'like', "$name_pattern%")
                ->orderBy('tariff.tariff_id', 'asc')
                ->get()->last();
        if ($last_similiar_tariff) {
            $last_similiar_id = $last_similiar_tariff->ID;
        } else {
            $last_similiar_id = 0;
        }
        if ($last_similiar_id) {
            $max_id = (int) $last_similiar_id;
            $id = ++$max_id;
            return (string)$id;
        } else {
            $last_id = DB::table('tariff')
                    ->select('tariff_id as ID')
                    ->orderBy('tariff_id', 'asc')
                    ->get()->last()->ID;
            $integer_id = (int) $last_id;
            $tariff_number = (int) substr($last_id, 2, 2);
            $integer_id += 101 - $tariff_number;
            return $integer_id;
        }
    }

    public static function put(array $data = Array()) {
        DB::table('tariff')
            ->insert([
                'tariff_id' => Tariff::generateID($data['name']),
                'name' => $data['name'],
                'month_price' => $data['month_price'],
                'conditions' => $data['conditions']
            ]);
    }

    public static function update_row(array $data = Array(), array $options = Array()) {
        DB::table('tariff')
            ->where('tariff_id', '=', $data['id'])
            ->update([
                'name' => $data['name'],
                'month_price' => $data['month_price'],
                'conditions' => $data['conditions']
            ]);
    }

    public static function destroy($id) {
        DB::table('tariff')
                ->where('tariff_id', '=', $id)
                ->delete();
        DB::table('connection')
                ->where('tariff_id', '=', $id)
                ->update([
                    'tariff_id' => '1001'
                ]);
    }

    public static function _getColumnsToShow() {
        return [
            'ID', 'Name', 'Month_price', 'Conditions'
        ];
    }

    /*used for showing the whole list of connecitons
     * explicit for output option text
     * implicit for get() method
     */
    public static function _getSortingOptions() {
        return [
            'explicit' => ['tariff ID', 'tariff name', 'price'],
            'implicit' => ['tariff.tariff_id',
                'tariff.name',
                'tariff.month_price']
            ];
    }

    public static function _getColumnsToEdit(){
        return [
            /*
             * editability,     tag,     type (for input tag), min, max (for numeric input), maxlength (for text input and textarea)
             * title,      has_radio
             */
            'ID' => ['editability' => 'readonly', 'tag' => 'input', 'type' => 'text', 'maxlength' => '2',
                'title' =>[], 'has_radio' => false],
            'Name' => ['editability' => 'editable', 'tag' => 'input', 'type' => 'text', 'maxlength' => '30',
                'title' =>[], 'has_radio' => false],
            'Month_price' => ['editability' => 'editable', 'tag' => 'input', 'type' => 'number', 'min' => '50', 'max' => '3000',
                'title' =>[], 'has_radio' => false],
            'Conditions' => ['editability' => 'editable', 'tag' => 'textarea', 'maxlength' => '300',
                'title' =>[], 'has_radio' => false],
        ];
    }

    public static function _getDataFieldsToEdit(){
        return ['id', 'name', 'month_price', 'conditions'];
    }

    public static function _getDataSecondaryToEdit(){
        return [];
    }
    public static function _getOptionsFieldsToEdit(){
        return [];
    }

    public static function _getColumnsToAdd(){
        return [
            'Name' => ['editability' => 'editable', 'tag' => 'input', 'type' => 'text',  'maxlength' => '30',
                'title' =>[], 'has_radio' => false],
            'Month_price' => ['editability' => 'editable', 'tag' => 'input', 'type' => 'number', 'min' => '50', 'max' => '3000',
                'title' =>[], 'has_radio' => false],
            'Conditions' => ['editability' => 'editable', 'tag' => 'textarea', 'maxlength' => '300',
                'title' =>[], 'has_radio' => false]
        ];
    }
    public static function _getDataSecondaryToAdd(){
        return [];
    }

    public static function _getDataFieldsToAdd() {
        return ['name', 'month_price', 'conditions'];
    }

    public static function _getOptionsFieldsToAdd() {
        return [];
    }
}
