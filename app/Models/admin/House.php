<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;

class House extends _SysAdmin
{
    use HasFactory;
    
    public static function get($column = 0, $direction = 0) {
        $ordering_options = House::_getSortingOptions()['implicit'];
        $column_prepared = in_array($column, [0, 1])? $column : 0;
        $direction_prepared = in_array($direction, ['asc', 'desc'])? $direction : 'asc';
        return DB::table('house')
                ->select('house_id as ID',
                        'street as Street',
                        'house_number as House_number')
                ->orderBy($ordering_options[$column_prepared], $direction_prepared)
                ->orderBy('house_number', 'asc')
                ->get();
    }
    
    public static function getOneByID($id){
        if(!$id) return null;
        $house = DB::table('house')
                ->select('street as Street',
                        'house_number as House_number',
                        'house_id as ID')
                ->where('house_id', $id)
                ->get()->first();
        $house->Connection_count = DB::table('connection')
                ->where('house_id', '=', $id)
                ->count();
        return $house;
    }
    
    public static function checkHousePresence($street, $house_number) {
        $present = DB::table('house')
                ->where([
                    ['street', '=', $street],
                    ['house_id', '=', $house_number]
                ])
                ->count();
        return $present == 0 ? false : true;
    }
    
    public static function getStreetsList() {
        return DB::table('houses')
                ->select('street')
                ->groupBy('street')
                ->get();
    }
    
    public static function put(array $data = Array(), array $options = Array()) {
        switch ($options['street_type']) {
            case 'old':
                DB::table('house')
                    ->insert([
                        'street' => $data['street_old'],
                        'house_number' => $data['house_number']
                    ]);
                break;
            case 'new':
                if (!empty($data['street_new'])) {
                    DB::table('house')
                        ->insert([
                            'street' => $data['street_new'],
                            'house_number' => $data['house_number']
                        ]);
                }
                break;
        }
    }
    
    public static function update_row(array $data = array(), array $options = array()) {
        switch ($options['street_type']) {
            case 'old':
                DB::table('house')
                    ->where('house_id', '=', $data['house_id'])
                    ->update([
                        'street' => $data['street_old'],
                        'house_number' => $data['house_number']
                    ]);
                break;
            case 'new':
                if (!empty($data['street_new'])) {
                    DB::table('house')
                        ->where('house_id', '=', $data['house_id'])
                        ->update([
                            'street' => $data['street_new'],
                            'house_number' => $data['house_number']
                        ]);
                }
                break;
            default:
                DB::table('house')
                    ->where('house_id', '=', $data['house_id'])
                    ->update(['house_number' => $data['house_number']]);
                break;
        }
    }
    
    public static function destroy($id) {
        DB::table('house')
                ->where('house_id', '=', $id)
                ->delete();
    }
    
    public static function _getColumnsToAdd() {
        return [];
    }
    
    public static function _getDataFieldsToAdd() {
        return [];
    }
    
    public static function _getDataSecondaryToAdd() {
        return [];
    }
    
    public static function _getOptionsFieldsToAdd() {
        return [];
    }

    public static function _getColumnsToEdit() {
        
    }

    public static function _getDataSecondaryToEdit() {
        
    }
    
    public static function _getDataFieldsToEdit() {
        return ['house_id', 'street_old', 'street_new', 'house_number'];
    }

    public static function _getOptionsFieldsToEdit() {
        return ['street_type'];
    }
    
    public static function _getColumnsToShow() {
        return [
            'ID', 'Street', 'House_number'
        ];
    }

    public static function _getSortingOptions() {
        return [
            'explicit' => ['house id', 'street'],
            'implicit' => ['house.house_id', 'house.street']
        ];
    }

}
