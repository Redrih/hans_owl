<?php

namespace App\Models\admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

abstract class _SysAdmin extends Model implements _AdminInterface, _AdminResourceInterface
{
    use HasFactory;
    
    public static function _splitRequest(Request $request, array $data_fields = Array(), array $options_fields = Array()) {
        $data = array();
        foreach ($data_fields as $field_name) {
            $data[$field_name] = $request->input($field_name);
        }
        $options = array();
        foreach ($options_fields as $field_name) {
            $options[$field_name] = $request->input($field_name);
        }
        return ['data' => $data, 'options' => $options];
    }
}
