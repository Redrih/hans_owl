<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\admin\Tariff;
use App\Models\user\Tariffs;

class TariffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $column = $request->input('column') ? $request->input('column') : 0;
        $direction = $request->input('direction') ? $request->input('direction') : 'asc';
        return view('admin.tariff.show', [
                'data_type' => 'Tariff',
                'sorting_options' => Tariff::_getSortingOptions(),
                'columns' => Tariff::_getColumnsToShow(),
                'data' => Tariff::get($column, $direction),
                'edit' => true,
                'delete' => true,
                'selectivity' => false
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tariff.add', [
            'data_type' => 'tariff',
            'columns' => Tariff::_getColumnsToAdd(),
            'data_secondary' => Tariff::_getDataSecondaryToAdd()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $store_data = Tariff::_splitRequest($request, Tariff::_getDataFieldsToAdd(), Tariff::_getOptionsFieldsToAdd());
        Tariff::put($store_data['data']);
        return Redirect::to('/admin/tariffs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($tariff_id)
    {
        return view("admin.tariff.show_single", [
            'tariff' => Tariff::getOneByID($tariff_id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.tariff.edit', [
            'data_type' => 'Tariff',
            'columns' => Tariff::_getColumnsToEdit(),
            'data' => Tariffs::getTariffSingleById($id),
            'data_secondary' => Tariff::_getDataSecondaryToEdit()
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $split = Tariff::_splitRequest($request, Tariff::_getDataFieldsToEdit(), Tariff::_getOptionsFieldsToEdit());
        Tariff::update_row($split['data'], $split['options']);
        return Redirect::to('/admin/tariffs');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tariff::destroy($id);
        return Redirect::to('/admin/tariffs');
    }
}
