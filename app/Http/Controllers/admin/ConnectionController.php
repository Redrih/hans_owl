<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Models\admin\Connection;
use App\Models\user\Connections;

class ConnectionController extends \App\Http\Controllers\Controller {
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $column = $request->input('column') ? $request->input('column') : 0;
        $direction = $request->input('direction') ? $request->input('direction') : 'asc';
        return view('admin.connection.show', [
            'data_type' => 'Connection',
            'sorting_options' => Connection::_getSortingOptions(),
            'columns' => Connection::_getColumnsToShow(),
            'data' => Connection::get($column, $direction),
            'edit' => false,
            'delete' => false,
            'selectivity' => false
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.connection.add', [
            'data_type' => 'connection',
            'columns' => Connection::_getColumnsToAdd()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $update_data = Connection::_splitRequest($request);
        Connection::put($update_data['data'], $update_data['options']);
        return Redirect::to('/admin/connections');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $connection = Connection::getOneById($id);
        return view("admin.connection.show_single", [
            'connection' => $connection
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.connection.edit', [
            'data_type' => 'Connection',
            'columns' => Connection::_getColumnsToEdit(),
            'data' => Connections::getConnectionSingleById($id),
            'data_secondary' => Connection::_getDataSecondaryFieldsToEdit()
                ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Connection::update_row($request);
        return Redirect::to('/admin/connections');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Connection::destroy($id);
        return Redirect::to('/admin/connections');
    }
}
