<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\admin\User;
use Illuminate\Http\Request;
use App\Models\user\Connections;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $column = $request->input('column') ? $request->input('column') : 0;
        $direction = $request->input('direction') ? $request->input('direction') : 'asc';
        return view('admin.user.show', [
                'data_type' => 'User',
                'sorting_options' => User::_getSortingOptions(),
                'columns' => User::_getColumnsToShow(),
                'data' => User::get($column, $direction),
                'edit' => true,
                'delete' => false,
                'roles' => ['admin', 'moder_tariff', 'moder_connection', 'client'],
                'selectivity' => true
            ]);        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\admin\User  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
        return view('admin.user.show_single', [
            'user' => User::getUserSingleByID($user_id),
            'connections' => Connections::getConnectionsByClient($user_id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\admin\User  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function edit($user_id)
    {
        return view('admin.user.edit', [
        'data_type' => 'User',
        'columns' => User::_getColumnsToEdit(),
        'data' => User::getUserSingleByID($user_id),
        'data_secondary' => User::_getDataSecondaryToEdit()
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\admin\User  $adminUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $adminUser)
    {
        $split = User::_splitRequest($request, User::_getDataFieldsToEdit(), User::_getOptionsFieldsToEdit());
        User::update_row($split['data'], $split['options']);
        return Redirect::to('/admin/users');
    }
}
