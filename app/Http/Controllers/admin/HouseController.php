<?php

namespace App\Http\Controllers\admin;

use App\Models\admin\House;
use App\Http\Controllers\Controller;
use App\Models\admin\Connection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $column = $request->input('column') ? $request->input('column') : 0;
        $direction = $request->input('direction') ? $request->input('direction') : 'asc';
        return view('admin.house.show', [
            'data_type' => 'House',
            'sorting_options' => House::_getSortingOptions(),
            'columns' => House::_getColumnsToShow(),
            'data' => House::get($column, $direction),
            'edit' => false,
            'delete' => false,
            'selectivity' => false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($house_id)
    {
        return view('admin.house.show_single', [
            'house' => House::getOneByID($house_id),
            'connections' => Connection::getAllByHouseId($house_id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about_house = House::getOneByID($id);
        return view('admin.house.edit', [
            'house' => $about_house['house'],
            'connections_count' => $about_house['connections_count'],
            'streets' => House::getStreetsList()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_data = House::splitRequest($request);
        $model_ad_house = new House();
        $model_ad_house->update($update_data['data'], $update_data['options']);
        return Redirect::to('/admin/houses');
    }
}
