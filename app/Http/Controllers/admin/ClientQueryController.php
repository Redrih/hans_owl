<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\admin\ClientQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ClientQueryController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\admin\ClientQuery  $clientQuery
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('admin.client_query', [
                'query' => ClientQuery::getFirst()
                ]);
    }
    
    public function perform(Request $request, $id) {
        ClientQuery::perform($id, $request->input('action'));
        return Redirect::to('admin/queries');
    }
}
