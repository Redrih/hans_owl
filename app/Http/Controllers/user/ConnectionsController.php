<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Models\user\Connections;
use App\Models\user\Houses;
use App\Http\Controllers\Controller;
use App\Models\user\Tariffs;
use Illuminate\Support\Facades\Auth;

class ConnectionsController extends Controller
{    
    public function showConnectionSingleById($id) {
        return view('provider.connections.connection_single',
                ['connection' => Connections::getConnectionSingleById($id)]);
    }
    
    public function showConnectionsByClientId() {
        return view('provider.connections.connections',
                ['connections' => Connections::getConnectionsByClient(Auth::user()->getAuthIdentifier()),
                    'houses' => Houses::getHousesAll()]);
    }
    
    public function askForConnection() {
        return view('provider.connections.ask_for_connection',
                ['houses' => Houses::getHousesAll(),
                    'tariffs' => Tariffs::getTariffsAll()]);
    }
    
    public function askForDisconnection() {
        return view('provider.connections.ask_for_disconnection',
                ['connections' => Connections::getConnectionsByClient(Auth::user()->getAuthIdentifier())]);
    }
    
    public function askForEditingConnection(Request $request, $id) {
        return view('provider.connections.ask_for_editing_connection', [
            'connection' => Connections::getConnectionSingleById($id),
            'houses' => Houses::getHousesAll(),
            'tariffs' => Tariffs::getTariffsAll()
        ]);
    }
    
    public function storeQuery(Request $request, $type) {
        Connections::sendQuery($request, $type);
        return redirect('/connections');
    }
}
