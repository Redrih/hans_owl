<?php

namespace App\Http\Controllers\user;

use App\Models\user\Tariffs;
use App\Http\Controllers\Controller;

class TariffsController extends Controller
{
    public function showTariffsAll() {
        $tariffs = Tariffs::getTariffsAll();
        return view('provider.tariffs.tariffs', ['tariffs' => $tariffs]);
    }
    
    public function showTariffSingleById($id) {
        $tariffs = Tariffs::getTariffSingleById($id);
        return view('provider.tariffs.tariff_single', ['tariffs' => $tariffs]);
    }
}
