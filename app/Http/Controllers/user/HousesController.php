<?php

namespace App\Http\Controllers\user;

use Illuminate\Http\Request;
use App\Models\Houses;
use App\Http\Controllers\Controller;

class HousesController extends Controller
{
    public function showHousesAll() {
        $houses = Houses::getHousesAll();
        $streets = Houses::getStreetsList();
        return view('provider.houses.houses', ['houses' => $houses, 'streets' => $streets]);
    }
    
    public function showHouseSingleById($id) {
        $houses = Houses::getHouseSingleById($id);
        $connections_count = Houses::countConnectionsInHouse($id);
        return view('provider.houses.house_single', ['house' => $houses, 'connections_count' => $connections_count]);
    }   
}
