<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Role {
    public function handle($request, Closure $next, string $roles) {
        if (!Auth::check()) {
            // This isnt necessary, it should be part of your 'auth' middleware
            return Redirect::to('/login');
        }
        $user = Auth::user();
        
        $roles_array = explode('|', $roles);
        
        foreach ($roles_array as $role){
            if ($user->role == $role){
                return $next($request);
            }
        }
        return Redirect::to('/login');
    }
}
