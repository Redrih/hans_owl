<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  ...$guards
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        if (Auth::guard($guard)->check()) {
        $role = Auth::user()->role; 

        switch ($role) {
            case 'admin':
                return redirect('/admin/users');
            case 'moder_connection':
                return redirect('/admin/connections');
            case 'moder_tariff':
                return redirect('/admin/tariffs');
            case 'client':
                return redirect('/main');
            default:
                return redirect('/main');
            }
        }
        return $next($request);
    }
}
