@extends('layouts.user.layout')

@section('page_title')
    <b>Servised houses</b>
@endsection

@section('content')
<form method="get" action="/houses">
    @csrf
    <select name="street">
        <option value="all">all streets</option>
        @foreach($streets as $street)
        <option value="{{$street->street}}"
                <?php if (isset($_GET['street']) && $_GET['street'] == $street->street) : ?>
                selected
                <?php endif; ?>>
        {{ $street->street }}
        </option>
        @endforeach
    </select>   
    <input type="submit" value="Show">
</form>


<table border="1">
    <th>Street</th>
    <th>House number</th>
        @foreach ($houses as $house)
        <?php if (isset($_GET['street']) && ($_GET['street'] == 'all' || $house->Street == $_GET['street'])) : ?>
        <tr align="center">
            <td>{{$house->Street}}</td>
            <td><a href='/houses/id={{$house->ID}}'>{{$house->Number}}</a></td>
        </tr>
        <?php endif; ?>
        @endforeach
</table>
<i style="margin-left: 100px;">Click the house number for details</i><br>
<a href='/main'>Back to the main page</a>
@endsection 