@extends('layouts.user.layout')

@section('style')
<link rel='stylesheet'
            type='text/css'
            href="<?php echo asset('css/provider/connect_form.css');?>">
@endsection

@section('page_title')
<b>Information about given house</b>
<p><?php use Illuminate\Support\Facades\Storage;
    echo Storage::path('connect_form.css')  ?></p>
@endsection

@section('content')
<div>
    <p>Identifier: {{$house->ID}}</p>
    <p>Street: {{$house->Street}}</p>
    <p>Number of the house: {{$house->Number}}</p>

    <p>Connections in the house: {{$connections_count}}</p>
    <a href='/houses/all'>Back to whole houses list</a><br>
    <a href='/main'>To the main page</a>
</div>
@endsection

