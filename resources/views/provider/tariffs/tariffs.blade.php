@extends('layouts.user.layout')

@section('page_title')
    <div class="page_title">
        <b>Best tariffs ever!</b>
    </div>
@endsection

@section('content')
<div class="content_data">
    <table class="content_data_table" border="1">
        <th>Name</th>
        <th>Price per month (in hryvnas)</th>
        @foreach ($tariffs as $tariff)
            <tr align="center">
                <td><a href="/tariffs/id={{$tariff->ID}}">{{$tariff->Name}}</a></td>
                <td>{{$tariff->Month_price}}</td>
            </tr>
        @endforeach
    </table>
    <i id="content_hint">Click the tariff ID for details</i><br>
    <?php use Illuminate\Support\Facades\Auth;
    if (Auth::user()) : ?>
    <a href='/main'>Back to the main page</a>
    <?php endif; ?>
</div>
@endsection
