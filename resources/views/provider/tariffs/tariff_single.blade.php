@extends('layouts.user.layout')

@section('page_title')
    <b>{{$tariffs->Name}}</b>
@endsection

@section('content')
    <p>Identifier: {{$tariffs->ID}}</p>
    <p>Tariff's name: {{$tariffs->Name}}</p>
    <p>Price per month: {{$tariffs->Month_price}}</p>
    <p>Conditions: {{$tariffs->Conditions}}</p>
    <a href='/tariffs'>Back to whole tariffs list</a><br>
    <?php use Illuminate\Support\Facades\Auth;
if (Auth::user()) : ?>
    <a href='/main'>To the main page</a>
<?php endif; ?>
@endsection

