@extends('layouts.user.layout')

@section('page_title')
<b id="page_title">Profitable Internet and Media Provider (PIMP)</b>
@endsection

@section('content')
<ul class="content_options">
    <li><a href="connections/connect">Ask for a new connection</a></li>
    <li><a href="connections/disconnect">Shut a connection down</a></li>
</ul>
<p id="content_my_con"><a href="connections">My connections</a></p>
<p id="content_tariffs"><a href='tariffs'>Take a look on the tariff list! Only best offers here!</a></p>
@endsection
