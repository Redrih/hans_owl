@extends('layouts.user.layout')

@section('page_title')
@endsection

@section('content')
<form method='post'
      action='/connections/store/edit'>
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <input type='text'
           hidden
           name='connection_id'
           value='{{$connection->ID}}'>
    <p>
        <span>House</span>
    </p>
    <p>
        <input type="radio"
               name="house_type"
               value="old"
               checked>
        <label> choose from list </label>
        <select name='house_id'>
            @foreach($houses as $house)
            <option value="{{$house->ID}}">
                {{$house->Street}} {{$house->House_number}}
            </option>
            @endforeach
        </select>
    </p>
    <p>
        <input type="radio"
               name="house_type"
               value="new"
               checked>
        <label> add new </label><br>
        <label>Street </label>
        <input name='street'
               type='text'>
        <label> House number </label>
        <input name="house_number"
               type="number"
               min="1"
               max="50">
    </p>
    <p>
        <span>Flat </span>
        <input name="flat"
               type="number"
               min="1"
               max="99">
    </p>
    <p>
        <span>Tariff </span>
        <select name="tariff_id">
            @foreach($tariffs as $tariff)
            <option value="{{$tariff->ID}}">
                {{$tariff->Name}}
            </option>
            @endforeach
        </select>
    </p>
    <input type='submit' value='Send'>
</form>
@endsection