@extends ('layouts.user.layout')

@section('page_title')
<b>Connected users here</b>
@endsection

@section('content')
<form method="get" action="connections">
    @csrf
    <select name="house">
        <option value="all">all houses</option>
        @foreach($houses as $house)
        <option value="{{$house->ID}}"
                <?php if (isset($_GET['house']) && $_GET['house'] == $house->ID) : ?>
                selected
                <?php endif; ?>>
        {{ $house->Street }} №{{ $house->House_number}}
        </option>
        @endforeach
    </select>   
    <input type="submit" value="Show" />
</form>

<table border="1">
    <th>Connection ID</th>
    <th>Street</th>
    <th>House number</th>
    <th>Tariff</th>
        @foreach ($connections as $connection)
        <?php if (isset($_GET['house']) && ($_GET['house'] == 'all' || $connection->House_ID == $_GET['house'])) : ?>
        <tr align="center">
            <td><a href="connections/id={{$connection->ID}}">{{$connection->ID}}</a></td>
            <td>{{$connection->Street}}</td>
            <td>{{$connection->House_number}}</td>
            <td title="month price : {{$connection->Month_price}}
conditions: {{$connection->Conditions}}">{{$connection->Tariff_name}}</td>
            <td><a href='/connections/edit/{{$connection->ID}}'>ask for editing</a></td>
        </tr>
        <?php endif; ?>
        @endforeach
</table><br>
<i style="margin-left: 100px;">Click the surname for details</i><br>
<a href='/main'>Back to the main page</a>
@endsection
