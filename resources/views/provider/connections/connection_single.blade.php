@extends('layouts.user.layout')

@section('page_title')
@endsection

@section('content')
<div>
    <p>Identifier: {{$connection->ID}}</p>
    <p>Client's ID: {{$connection->Client_ID}}</p>
    <p>Street: {{$connection->Street}}</p>
    <p>Number of the house: {{$connection->House_number}}</p>
    <a href='/connections'>Back to whole connections list</a><br>
    <a href='/main'>To the main page</a>
</div>
@endsection
