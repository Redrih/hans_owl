@extends('layouts.user.layout')

@section('page_title')

@endsection

@section('content')
<p>Asking for disconnection</p>
<form method='post'
      action='/connections/store/disconnect'>
    {{method_field('PUT')}}
    {{csrf_field()}}
    <table border="1">
        <th></th>
        <th>Adress</th>
        <th>Tariff</th>
    @foreach($connections as $connection)
    <tr>
        <td><input name="{{$connection->ID}}"
                   type="checkbox"
                   value='1'></td>
        <td>{{$connection->Street}} {{$connection->House_number}}, flat {{$connection->Flat}}</td>
        <td name="{{$connection->Tariff_ID}}"
            title="{{$connection->Month_price}}\n{{$connection->Conditions}}">{{$connection->Tariff_name}}</td>
    </tr>
    @endforeach
    </table>
    <input type='submit' value="Send"><br>
    <a href='/main'><input type='button' value='Back to main page'></a>
</form>
@endsection