@extends('layouts.user.layout')

@section('page_title')
<p>Make an order for new connection</p>
@endsection

@section('content')
<form method='post'
      action='/connections/store/connect'>
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <p>House </p>
    <p>
        <input name="house_type"
            type="radio"
            value="old"
            checked>
        <span>choose from the list </span>
        <select name="house_id">
            @foreach($houses as $house)
            <option value="{{$house->ID}}">
                {{$house->Street}} {{$house->House_number}}
            </option>
            @endforeach
        </select>
    </p>
    <p>
        <input type="radio"
               name="house_type"
               value="new" />
        <span>add a new one: </span>
        <br>
        <span>street </span>
        <input type="text"
               name="street"
               placeholder="Floydstreet">
        <span><!--king--> house_number </span>
        <input name="house_number"
               type="number"
               min="1"
               max="50">
    </p>
    <p>
        <span>Flat </span>
        <input name="flat"
               type="number"
               min="1"
               max="99">
    </p>
    <p>
        <span>Tariff </span>
        <select name="tariff_id">
            @foreach($tariffs as $tariff)
            <option value="{{$tariff->ID}}"
                    title="month price: {{$tariff->Month_price}}
conditions: {{$tariff->Conditions}}">
                {{$tariff->Name}}
            </option>
            @endforeach
        </select>
    </p>
    
    <input type="submit"
           value="Send">
</form>
<a href="/main">
<input type="button" value="Cancel (back to main page)">
</a>
@endsection