@extends('layouts.admin.layout')

@section('content')
<p>Connection ID : {{$connection->ID}}</p>
<p>Client ID : {{$connection->Client_ID}}</p>
<p>House ID : {{$connection->House_ID}}</p>
<p>Street : {{$connection->Street}}</p>
<p>House number : {{$connection->House_number}}</p>
<p>Flat : {{$connection->Flat}}</p>
<p>Tariff ID : {{$connection->Tariff_ID}}</p>
<p>Tariff name : {{$connection->Name}}</p>
<p>Month price : {{$connection->Month_price}}</p>
<p><del>slavery</del> conditions : {{$connection->Conditions}}</p>
<a href="/admin/connections"><input type="button" value="Back to connections list"></a>
@endsection

