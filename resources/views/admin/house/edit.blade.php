@extends('layouts.admin_layout')

<style type="text/css">
label {
min-width: 150px;
display: inline-block;
}
</style>

@section('content')
<h2>Edit house</h2>
<form action="/admin/houses/{{ $house->ID }}" method="POST">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <label>House ID: </label>
    <input name='house_id'  
        type='text'
        readonly
        value='{{$house->ID}}'>
    <br><br>
    <label>Street: </label>
    <input name="street_type"
            type="radio"
            value="old"
            <?php if (isset($_GET['street_type']) && $_GET['street_type'] == "old" ||
                    !isset($_GET['street_type'])) : ?>
            checked
            <?php endif; ?>>
    <label>Choose from the list</label>
    <select name="street_old">
        @foreach ($streets as $street)
        <option value='{{$street->street}}'
                <?php if ($street->street == $house->Street) : ?>
                selected
                <?php endif; ?>>
            {{$street->street}}
        </option>
        @endforeach
    </select>
    <input name="street_type"
            type="radio"
            value="new"
            <?php if (isset($_GET['street_type']) && $_GET['street_type'] == "new") : ?>
            checked
            <?php endif; ?>>
    <span> add a new street </span>
    <input name="street_new"
           type="text"
           editable
           maxlength="30"
           placeholder="GeorgeNotFoundStreet">
    <br><br>
    <label>House number: </label>
        <input name="house_number"
                type="number"
                min="1"
                max="50"
                value="<?php if (isset($_GET['house_number'])) {
                    echo $_GET['house_number'];
                }else {
                    echo $house->House_number;}?>">
    <br><br>
    <input type="submit" value="Save">
</form>
@endsection