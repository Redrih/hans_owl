@extends('layouts.admin.layout')

@section('content')
<p>House ID: {{$house ->ID}}</p>
<p>Street : {{$house->Street}}</p>
<p>House number : {{$house->House_number}}</p>
<p><b>Connections in the house : </b></p>
<table border='1'>
    <th>Connection ID</th>
    <th>User</th>
    <th>Flat</th>
    <th>Tariff</th>
    @foreach($connections as $connection)
    <tr>
        <td>{{$connection->ID}}</td>
        <td title='client ID : {{$connection->Client_ID}}'>{{$connection->Login}}</td>
        <td>{{$connection->Flat}}</td>
        <td title='tariff ID : {{$connection->Tariff_ID}}
month price : {{$connection->Month_price}}
conditions : {{$connection->Conditions}}'>{{$connection->Name}}</td>
    </tr>
    @endforeach
</table>
<p><a href='/admin/houses'><input type='button' value='Back to houses list'></a></p>
@endsection

