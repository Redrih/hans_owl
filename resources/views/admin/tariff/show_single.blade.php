@extends('layouts.admin.layout')

@section('content')
<p>Tariff ID : {{$tariff->ID}}</p>
<p>Tariff name : {{$tariff->Name}}</p>
<p>Month price : {{$tariff->Month_price}}</p>
<p><del>slavery</del> conditions : {{$tariff->Conditions}}</p>
<a href="/admin/tariffs"><input type="button" value="Back to tariffs list"></a>
@endsection

