@extends('layouts.admin.layout')

@section('content')
<p>User ID: {{$user->ID}}</p>
<p>Login: {{$user->Login}}</p>
<p>E-mail: {{$user->Email}}</p>
<p>Role: {{$user->Role}}</p>
@if (($user->Role) == 'client' && count($connections) != 0)
<table>
    <th>Connection ID</th>
    <th>House ID</th>
    <th>Street</th>
    <th>House number</th>
    <th>Flat</th>
    <th>Tariff</th>
    @foreach($connections as $connection)
    <tr>
        <td>{{$connection->ID}}</td>
        <td>{{$connection->House_ID}}</td>
        <td>{{$connection->Street}}</td>
        <td>{{$connection->House_number}}</td>
        <td>{{$connection->Flat}}</td>
        <td title='{{$connection->Tariff_ID}}\n
            {{$connection->Month_price}}\n
            {{$connection->Conditions}}'>{{$connection->Tariff_name}}</td>
    </tr>
    @endforeach
</table>
@else
<p>No connectins for this client (mb kick?)</p>
@endif
@endsection