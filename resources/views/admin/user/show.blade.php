@extends('layouts.admin._show_layout')

@section('additional_form_________')
<form method="get" action="users">
    @csrf
    <select name="essential">
        <option value="all">with any role</option>
        @foreach($roles as $role)
        <option value="{{$role}}"
                <?php if (isset($_GET['role']) && $_GET['role'] == $role) : ?>
                selected
                <?php endif; ?>>{{$role}}</option>
        @endforeach
    </select>
    <input type="submit" value="Show" />
</form>
<br><br>
@endsection
