@extends('layouts.admin.layout')

@section('content')
@if($query)
<h3>To do: {{$query->Query_type}}</h3>
<table border='1'>
    <th>Client ID</th>
    <th>Client login</th>
@switch($query->Query_type)
    @case('connect')
        @if($query->House_ID)
            <th>House_ID</th>
        @elseif($query->Street)
            <th>Street</th>
            <th>House number</th>
        @endif
        <th>Flat</th>
        <th>Tariff</th>
        <tr align="center">
            <td>{{$query->Client_ID}}</td>
            <td>{{$query->Login}}</td>
            @if($query->House_ID)
                <td>{{$query->House_ID}}</td>
            @elseif($query->Street)
                <td>{{$query->Street}}</td>
                <td>{{$query->House_number}}</td>
            @endif
            <td>{{$query->Flat}}</td>
            <td>{{$query->Tariff_ID}}</td>
        </tr>
    @break
    @case('disconnect')
        <th>Connection ID</th>
        <th>Tariff</th>
        <tr align="center">
            <td>{{$query->Client_ID}}</td>
            <td>{{$query->Login}}</td>
            <td>{{$query->Connection_ID}}</td>
            <td>{{$query->Tariff_ID}}</td>
        </tr>
    @break
    @case('edit')
    <th>Connection ID</th>
    <th>Old tariff</th>
    <th>New tariff</th>
    @if($query->House_ID)
    <th>Old house ID</th>
    <th>New house ID</th>
    @elseif($query->Street)
    <th>Old house ID</th>
    <th>New house street</th>
    <th>New house number</th>
    @endif
    <tr align="center">
        <td>{{$query->Client_ID}}</td>
        <td>{{$query->Login}}</td>
        <td>{{$query->Connection_ID}}</td>
        <td>{{$query->Old_tariff}}</td>
        <td>{{$query->Tariff_ID}}</td>
        @if($query->House_ID)
        <td>{{$query->Old_house}}</td>
        <td>{{$query->House_ID}}</td>
        @elseif($query->Street)
        <td>{{$query->Old_house}}</td>
        <td>{{$query->Street}}</td>
        <td>{{$query->House_number}}</td>
        @endif
    </tr>
    @break
    @endswitch
</table>
<form method="post" action="/admin/queries/perform/{{$query->Query_ID}}">
     {{ method_field('PUT') }}
    {{ csrf_field() }}
    <input type="radio" name="action" value="accept" checked="checked" />
    <label>Accept</label><br>
    <input type="radio" name="action" value="decline"/>
    <label>Decline</label>
    <p><input type="submit" value="Do" /></p>   
</form>
@else
<p>No queries at the moment</p>
@endif
<a href="/admin/connections"><input type="button" value="Back to connections list"></a>
@endsection