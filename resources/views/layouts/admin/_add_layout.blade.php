@extends('layouts.admin.layout')

<style type="text/css">
    label {
    min-width: 150px;
    display: inline-block;
    }
</style>

<!--
Variables:
string $data_type               is in ['tariff', 'user'] - for correct linking and naming inside given page
                                                    only lowercase here
array $columns                  represents the whole list of USED database table columns for given data type
array $data_secondary        contains data that's necessary for creating select-tags amd some other elements of edit-form
-->
@section('content')
    <h2>Add new {{$data_type}}</h2>
    <form action="/admin/{{$data_type}}s" method="POST">
        {{ csrf_field() }}
        <?php foreach ($columns as $column => $properties) : ?>
        <label>{{$column}} </label>

        <?php if($properties['has_radio']) :
            foreach($data_secondary['for_radio_tag'][$column] as $adding_way) : ?>
            <input name='{{$adding_way['name']}}'
                   type='radio'
                   value='{{$adding_way['value']}}'>
            <?php switch($adding_way['tag']) :
                case 'select' : ?>
                <select name='{{$adding_way['name']}}'>
                    <?php foreach ($data_secondary['for_select_tag'][(string)$column] as $column_instance) : ?>
                    <option value='{{$column_innstance ->ID}}'
                            title="{{$column_instance -> title}}">
                        {{$column_instance ->to_view}}
                    </option>
                    <?php endforeach; ?>
                </select>
                <?php break;
                case 'input' :
                    foreach($adding_way['input_tags'] as $tag_info) : ?>
                    <label>{{$tag_info['label']}} </label>
                    <input name='{{$tag_info['name']}}'
                           type='{{$tag_info['type']}}'
                           <?php if ($tag_info['type'] == 'number') : ?>
                           min='{{$tag_info['min']}}'
                           max='{{$tag_info['max']}}'
                           <?php endif; ?>
                           editable><br>
                    <?php endforeach;
                break;
            endswitch;
            endforeach;

        else :
            switch($properties['tag']) :
                case 'select' : ?>
                <select name='<?php echo strtolower($column);?>'>
                    <?php foreach ($data_secondary['for_select_tag'][(string)$column] as $column_instance) : ?>
                    <option value='{{$column_innstance ->ID}}'
                            title="{{$column_instance -> title}}">
                        {{$column_instance ->to_view}}
                    </option>
                    <?php endforeach; ?>
                </select>
                <?php break;
                case 'input' : ?>
                <input name='<?php echo strtolower($column)?>'
                    type='<?php echo $properties["type"]; ?>'
                       <?php echo $properties['editability'];?>
                    title="<?php foreach ($properties['title'] as $title_element) {
                        echo "$title_element\n";
                    }?>"
                    <?php if ($properties['type'] == 'number') : ?>
                    min='{{$properties['min']}}'
                    max='{{$properties['max']}}'
                    <?php elseif ($properties['type'] == 'text') : ?>
                    maxlength="{{$properties['maxlength']}}"
                    <?php endif; ?>>
                <?php break;
                case 'textarea' : ?>
                    <textarea name="<?php echo strtolower($column); ?>"
                              <?php echo $properties['editability']; ?>
                              maxlength="<?php echo $properties['maxlength']; ?>"></textarea>
                <?php break;
            endswitch;
        endif; ?>
        <br><br>
        <?php endforeach; ?>
        <input type="submit" value="Save">
    </form>
   @endsection