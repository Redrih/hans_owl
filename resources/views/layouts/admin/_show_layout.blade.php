@extends('layouts.admin.layout')

@section('content')
    <title>PIMP {{$data_type}}s List</title>
<!--
Variables:
string $data_type                           is in ['house', 'connection', 'tariff'] - for right linking and naming inside given page
array $sorting_options                    contains columns which values output data may be sorted by
array $columns                              represents the whole list of USED columns for given data type
array $data                                    contains all selected rows from the database for given data type
boolean $edit , $delete                    states if given resource type supposes editing and deleting by admin or moderators
-->
<div class="content_main">
    <div class="content_head">
        <h2 id="content_head_title"><?php echo "$data_type";?>s</h2>
        <div class="content_head_form">
            <form method='get' action='/admin/<?php echo strtolower($data_type);?>s'>
                <span id="sort_field_label">Sorting:</span>
                <select id="sort_field_select"
                    name="column">
                    <option value="0"
                            <?php if (!isset($_GET['column']) || $_GET['column'] == "0") : ?>
                            selected
                    <?php endif; ?>>
                        by <?php echo $sorting_options['explicit'][0]; ?>
                    </option>
                    <?php for ($i = 1; $i <= count($sorting_options['explicit']) - 1; $i++) : ?>
                    <option value="<?php echo $i; ?>"
                            <?php if (isset($_GET['column']) && $_GET['column'] == "$i") : ?>
                            selected
                    <?php endif; ?>>
                        by <?php echo $sorting_options['explicit'][$i]; ?>
                    </option>
                    <?php endfor; ?>
                </select>

                <span id="order_label">Order:</span>
                <input id="order_radio_asc"
                       value='asc'
                       type='radio'
                       name='direction'
                       <?php if (!isset($_GET['direction']) || $_GET['direction'] == "asc") : ?>
                       checked
                <?php endif; ?>>
                <span id="order_label_asc">ascending</span>
                <input id="order_radio_desc"
                       value='desc'
                       type='radio'
                       name='direction'
                       <?php if (isset($_GET['direction']) && $_GET['direction'] == "desc") : ?>
                       checked
                <?php endif; ?>>
                <span id="order_label_desc">descending</span>
                <input id="sort_button"
                       type='submit'
                       value='Sort'>
            </form>
        </div>
    </div>
    {{--
    @hasSection('additional_form')
    @yield('additional_form')
    @endif
    --}}

    <div class="content_data">
        <table class="content_data_table"
               border="1" style="text-align: center;">
            <?php for ($i = 0; $i <= count($columns) - 1; $i++) : ?>
            <th><?php echo $columns[$i]; ?></th>
            <?php endfor; ?>
            @foreach ($data as $data_element)
                <tr>
                    <td>
                        <a href="/admin/<?php echo strtolower("$data_type");?>s/{{ $data_element->ID }}"
                        >{{ $data_element->ID }}</a>
                    </td>
                    <?php for ($i = 1; $i <= count($columns) - 1; $i++) : ?>
                    <td><?php $column = $columns[$i];
                        echo $data_element->$column; ?></td>
                    <?php endfor; ?>
                    @if($edit || $delete)
                        <td border='0'>
                            @if($edit)
                                <a href="/admin/<?php echo strtolower($data_type);?>s/{{ $data_element->ID }}/edit">edit</a>
                            @endif
                            @if($delete)
                                <form style="float:right; padding: 0 15px;"
                                      action="/admin/<?php echo strtolower($data_type);?>s/{{ $data_element->ID }}"
                                      method="POST">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button>Delete</button>
                                </form>
                            @endif
                        </td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
</div>
@endsection
