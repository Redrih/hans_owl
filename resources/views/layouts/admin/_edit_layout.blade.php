@extends('layouts.admin.layout')

<style type="text/css">
label {
min-width: 150px;
display: inline-block;
}
</style>

@section('content')
<!--
Variables:
string $data_type               is in ['House', 'Connection', 'Tariff', 'User', 'Client_account'] - for correct linking and naming inside given page
                                                        first character is capital here
array $columns                  represents the whole list of USED columns for given data type
array $data                        contains all selected rows from the database for given data type
array $data_secondary        contains data that's necessary for creating select-tags amd some other elements of edit-form
-->
<h2>Edit <?php echo strtolower($data_type); ?></h2>
<form action="/admin/<?php echo strtolower($data_type);?>s/{{ $data->ID }}" method="POST">
    {{ method_field('PUT') }}
    {{ csrf_field() }}
    <?php foreach ($columns as $column => $properties) : ?>
    <label>{{$column}} </label>
    
    <?php if($properties['has_radio']) : ?>
        <?php foreach($data_secondary['for_radio_tag'][$column] as $editing_way) : ?>
        <input name='{{$editing_way['name']}}'
               type='radio'
               value='{{$editing_way['value']}}'>
        <?php switch($editing_way['tag']) :
            case 'select' : ?>
            <select name='{{$editing_way['name']}}'>
                <?php foreach ($data_secondary['for_select_tag'][(string)$column] as $column_instance) : ?>
                <option value='{{$column_innstance['ID']}}'
                        title="{{$column_instance ['title']}}">
                    {{$column_instance['to_view']}}
                </option>
                <?php endforeach; ?>
            </select>
            <?php break;
            case 'input' :
                foreach($editing_way['input_tags'] as $tag_info) : ?>
                <label>{{$tag_info['label']}} </label>
                <input name='{{$tag_info['name']}}'
                       type='{{$tag_info['type']}}'
                       <?php if ($tag_info['type'] == 'number') : ?>
                       min='{{$tag_info['min']}}'
                       max='{{$tag_info['max']}}'
                       <?php endif; ?>
                       editable><br>
                <?php endforeach;
                break;
        endswitch;
        endforeach;        
    else :
        switch($properties['tag']) :
            case 'select' :?>
            <select name='<?php echo strtolower($column);?>'>
                <?php foreach ($data_secondary['for_select_tag'][(string)$column] as $column_instance) : ?>
                <option value='{{$column_instance["ID"]}}'
                        title='{{$column_instance["title"]}}'>
                    {{$column_instance["to_view"]}}
                </option>
                <?php endforeach; ?>
            </select>
            <?php break;
            case 'input' :?>
            <input name='<?php echo strtolower($column)?>'
                type='<?php echo $properties["type"]; ?>'
                   <?php echo $properties['editability'];?>
                title="<?php if (count($properties['title'])) {
                    foreach ($properties['title'] as $title_element) {
                        echo "$title_element $data[$title_element]\n";
                    }};?>"
                <?php if ($properties['type'] == 'number') : ?>
                min='{{$properties['min']}}'
                max='{{$properties['max']}}'
                <?php elseif ($properties['type'] == 'text') : ?>
                maxlength="{{$properties['maxlength']}}"
                <?php endif; ?>
                value="{{$data->$column}}">
            <?php break;
            case 'textarea' : ?>
                <textarea name="<?php echo strtolower($column); ?>"
                          <?php echo $properties['editability']; ?>
                          maxlength="{{$properties['maxlength']}}"
                          >{{$data->$column}}
                </textarea>
                <?php break;
        endswitch;
    endif;?>
    <br><br>
    <?php endforeach; ?>
    <input type="submit" value="Save">
@endsection
