<html>
<head>
    <link rel="stylesheet" href="/css/admin/layout.css">
    <link rel="stylesheet" href="/css/admin/show_list.css">
    <link rel="stylesheet" href="/css/admin/show_single.css">
    <link rel="stylesheet" href="/css/admin/auth.css">
    <link rel="stylesheet" href="/css/common/footer.css">
</head>
<body link="#6d058d"
    alink="#bf001a"
    vlink="#000000">
<div class="leftnav">
    @include('layouts.logged_user')
    <div class="leftnav_text">
        <h1 id="leftnav_oddman_word">Odd-man</h1>
        <p id="leftnav_long_walk"><a href='/admin/main'>Long walk home, riddled with regret</a></p>
        <div class="leftnav_prov_system">
            <b id="leftnav_prov_system_title">Provider system</b>
            <ul class="leftnav_prov_system_lists">
                <li><a href="/admin/connections">Connections</a></li>
                <li><a href="/admin/houses">Houses</a></li>
                <li><a href="/admin/tariffs">Tariffs</a></li>
                <li><a href='/admin/users'>Users</a></li>
            </ul>
            <ul class="leftnav_prov_system_add">
                <li><a href="/admin/tariffs/create">Add new tariff</a></li>
            </ul>
        </div>
        <div class="leftnav_additional">
            <b id="leftnav_additional_title">Additional</b>
            <ul class="leftnav_additional_action">
                <li><a href='/admin/queries'>process some clients' queries (I'll be right happy to)</a></li>
            </ul>
        </div>
    </div>
</div>
<div class="content">
    @yield('content')
    @include('layouts.footer')
</div>
<br><br>

</body>
</html>
