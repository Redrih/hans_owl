<html>
    <head>
        <title>PIMP</title>
        <link rel="stylesheet" href="/css/user/auth.css">
        <link rel="stylesheet" href="/css/user/show.css">
        <link rel="stylesheet" href="/css/common/footer.css">
        <link rel="stylesheet" href="/css/user/header.css">
        <link rel="stylesheet" href="/css/user/main.css">
    </head>

    @include('layouts.header')
    <body>
    <div class="content">
        <div class="content_main">
            @yield('page_title')
            @yield('content')
        </div>
        @include('layouts.footer')
    </div>
    </body>
</html>


